# Promento solenimente não fazer nada de bom
# EXPLICAÇÃO BÁSICA

Git é uma ferramenta de versionamento de arquivos, desenvolvido por Linus Torvalds.

## Criada para ser:

* Gratuita e Open Source
* Distribuido (Conectividade não bloqueia o trabalho)
* Rápida
* Simples de fácil entendimento
* Suporte robusto a desenvolvimento paralelo (Branch)
* Totalmente distribuido
* Capaz de lidar com grandes projetos
  * Principal motivador para sua criação foi o projeto do Kernel do Linux

## Como funciona ?

O Git é separado em 2 repositórios principais:

* Local - o que fica localmente em uma máquina
* Remoto - o que fica em um servidor
  * Existem vários servidores providos por serviços de hospedagem, os mais conhecidos são: gitlab, github e bitbucket

Tanto no repositório local como remoto é possível salvar diferentes versões, que chamamos de commits, 
também é possível criar branches

Esta imagem tirada do vídeo [https://git-scm.com/video/what-is-git](url) é um exemplo visual de como o trabalho colaborativo pode ocorrer dentro de um projeto.

Mais de uma pessoa pode estar trabalhando na mesma atividade em partes diferentes ou iguais em tempos iguais ou diferentes e isso pode ser complexo de organizar, por este motivo o git disponibiliza a organização de branches e a possibilidade de unir esses trabalhos com merge. 

![Alt text](imgs/colaboration.png "colaboração")

Pense no git como uma árvore para, os galhos são os branches e quando nasce um galho que dá para fazer uma muda planta-se uma nova árvore a parti desse galho que chamamos de fork, a imagem a baixo exemplifica isso [https://www.theserverside.com/blog/Coffee-Talk-Java-News-Stories-and-Opinions/command-line-GitHub-fork-CLI-terminal-shell](url).

![Alt text](imgs/cdo-git_clone_vs_fork-f_desktop.png "fork")

Quando se chega a um versão consolidada considerada importante é possível demarcar esse estado usando tag, exemplo:

`git tag -a <tag_name> -m "my version 1.0"`

Para enviar uma tag para o repositório remoto

`git push <remote_name> <tag_name>`

Em todo projeto tem aquele conteúdo que é importante para a execução mas não para versionar para estes casos temos o .gitignore que nada mais é  do que uma lista do que queremos ignorar.

Para conhecer mais sobre o git veja:

<iframe width="640" height="360" src="https://web.microsoftstream.com/embed/video/a5e05e7f-7206-4bf8-b769-b8f1c62cf5db?autoplay=false&showinfo=true" allowfullscreen style="border:none;"></iframe>


[https://web.microsoftstream.com/video/a5e05e7f-7206-4bf8-b769-b8f1c62cf5db](url)

# ROTEIRO DO CURSO

## Requisitos

* Entender os conceitos básicos
* Ter o git instalado na máquina
* Ter uma IDE instalada na máquina de preferência o Visual Studio Code

## Inicializando repositório local git

Crie uma pasta chamada curso_git

Abra o terminal entre na pasta e execute o comando:

Obs: se não quiser criar a pasta antes só adicionar o nome que deseja no final do comando abaixo que ele cria para você

``` 
git init 

``` 
 

## Criando um branch

Crie branch com seu nome e ao mesmo tempo entre no branch executando o comando:


``` 
git checkout -b [SUBSTITUIR PELO SEU NOME COMPLETO SEPARADO POR _ EXEMPLO ana_caroline_ferreira]

``` 
 

## Criando o primeiro commit

Crie um arquivo chamado README.md

Dentro do arquivo escreve a frase "Prometo solenimente não fazer nada de bom"

Salve o arquivo

Adicione o arquivo para envio usando o comando:

``` 
git add --all
```



Salve a versão do seu projeto no branch executando o comando:

``` 
git commit -m "meu primeiro commit"
``` 

Isso força você a se identificar antes do pull então use os comandos abaixo para configurar o git informando quem você é:


```
git config user.name "[SUBSTITUA POR SEU NOME COMPLETO]"
git config user.email "[SUBSTITUA POR SEU EMAIL]"
```

Agora realize o commit 

## Resetando um commit

Edite o arquivo README.md e coloque um palavrão salve e dê um commit como explicado anteriormente mudando a frase meu primeiro commit para meu segundo commit

Vish se tá fudido! Você comitou um palavrão a agora ? 

Muita calma nessa hora, agora você vai aprender a desfazer essa merda

Primeiro liste os commits feitos executando o comando:

``` 
git log
``` 

Copie o identificador do commit cuja a mensagem é meu primeiro commit


``` 

commit 3db8dd8aa10f0bde0cff4bc2e62ed1a990e0c7cd
Author: Ana Caroline Ferreira <ana.ferreira@pti.org.br>
Date:   Wed Aug 30 15:25:54 2023 -0300

    meu primeiro commit


``` 

Agora execute o comando utilizando o identificado do commit que copiou:


``` 
git reset --hard 3db8dd8aa10f0bde0cff4bc2e62ed1a990e0c7cdgit
``` 

## Comunicando repositório local com remoto - para ter uma cópia online

Para adionar um repositório remoto execute o comando:


``` 
git remote add [SUBSTITUIR PELO NOME QUE VOCÊ QUISER DAR AO LINK REMOTO EXEMPLO origem] https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git
``` 

Obs: origem é o nome do link remoto que pode ser qualquer coisa pois você pode ter vários repositórios 
remotos para um repositório local se quiser.


## Subindo uma cópia do repositório local no remoto


OBS: Seu usuário e senha no gitlab será solicitado, se tudo deu certo um branch com seu nome foi criado no repositório do remoto do projeto no gitlab

## Copiando as referências do remoto para o local

Para copias as referências do remoto para o local execute:


```
git fetch --all

```

OBS: Seu usuário e senha no gitlab será solicitado

Para verificar se referencias dos branches remotos foram adicionadas ao repositório local execute o comando:


```
git branch -a

```

## Unindo o conteúdo do main remoto no branch do repositório local

Para trazer a versão mais recente do main para o branch local execute o comando:

```
git merge [SUBSTITUIR PELO NOME QUE VOCÊ DEU AO LINK REMOTO EXEMPLO origem]/main
```

OBS: Seu usuário e senha no gitlab será solicitado

Pode dar o problema "fatal: refusing to merge unrelated histories" para resolver usar --allow-unrelated-histories

Tente executar a união com o comando abaixo:

Obs: Ao invés do git merge poderiamos utilizar o rebase, porém esse comando meche na estrutura histórica da árvore de versionamento, criando um histórico linear oferencendo muitos riscos ao projeto, por isso não é recomendado utiliza-lo principalmente em branches públicos.

```
git merge --allow-unrelated-histories [SUBSTITUIR PELO NOME QUE VOCÊ DEU AO LINK REMOTO EXEMPLO origem]/main

```

Se tudo deu certo o arquivo README.md esta com conflito, em cima aparece o conteúdo do seu branch e em baixo o conteúdo do branch master

No caso para que esse projeto possa ser usado em outros cursos por favor, mantenha o que vem do main e descarte o as suas modificações no arquivo

E commit a sua descisão
E execute o comando:

```
git push [SUBSTITUIR PELO NOME QUE VOCÊ DEU AO LINK REMOTO EXEMPLO origem] [SUBSTITUIR PELO SEU NOME COMPLETO SEPARADO POR _ EXEMPLO ana_caroline_ferreira]

```

Agora crie uma pasta chamada coisas_a_ignorar

Depois crie um arquivo fora da pasta coisas_a_ignorar mas dentro da pasta curso_git, no nome do arquivo deve ser .gitignore

Edite o .gitignore e escreve dentro do arquivo coisas_a_ignorar/ e salve

Dentro da pasta coisas_a_ignorar crie um arquivo de meus_segredos_mais_profundos.txt

Adicione para envio via git

Salve via git 

E envie ao repositório via git
