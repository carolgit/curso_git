```
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git init
hint: Using 'master' as the name for the initial branch. This default branch name
hint: is subject to change. To configure the initial branch name to use in all
hint: of your new repositories, which will suppress this warning, call:
hint: 
hint:   git config --global init.defaultBranch <name>
hint: 
hint: Names commonly chosen instead of 'master' are 'main', 'trunk' and
hint: 'development'. The just-created branch can be renamed via this command:
hint: 
hint:   git branch -m <name>
Repositório vazio Git inicializado em  /home/ana/Área de Trabalho/curso-git/.git/
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git checkout -b ana_caroline_ferreira
Switched to a new branch 'ana_caroline_ferreira'
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git commit -m "meu primeiro commit"
No ramo ana_caroline_ferreira

Submissão inicial.

Arquivos não monitorados:
  (utilize "git add <arquivo>..." para incluir o que será submetido)
        README.md

nada adicionado ao envio mas arquivos não registrados estão presentes (use "git add" to registrar)
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git add --all
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git commit -m "meu primeiro commit"
[ana_caroline_ferreira (root-commit) 3db8dd8] meu primeiro commit
 1 file changed, 27 insertions(+)
 create mode 100644 README.md
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git add --all
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git commit -m "meu segundo commit"
[ana_caroline_ferreira 500f956] meu segundo commit
 1 file changed, 13 insertions(+)
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git logs
git: 'logs' não é um comando git. Veja 'git --help'.

The most similar command is
        log
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git log
commit 500f956fb328b42f58812da07dff53d86276b6e6 (HEAD -> ana_caroline_ferreira)
Author: Ana Caroline Ferreira <ana.ferreira@pti.org.br>
Date:   Wed Aug 30 15:26:13 2023 -0300

    meu segundo commit

commit 3db8dd8aa10f0bde0cff4bc2e62ed1a990e0c7cd
Author: Ana Caroline Ferreira <ana.ferreira@pti.org.br>
Date:   Wed Aug 30 15:25:54 2023 -0300

    meu primeiro commit
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ ^C
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git reset --hard 3db8dd8aa10f0bde0cff4bc2e62ed1a990e0c7cd
HEAD is now at 3db8dd8 meu primeiro commit
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git add --all
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git commit -m "readme part1"
[ana_caroline_ferreira 9a801d1] readme part1
 1 file changed, 47 insertions(+)
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git logs
git: 'logs' não é um comando git. Veja 'git --help'.

The most similar command is
        log
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git log
commit 9a801d19ba68255e2a2ff2d156679d638a8cc6f1 (HEAD -> ana_caroline_ferreira)
Author: Ana Caroline Ferreira <ana.ferreira@pti.org.br>
Date:   Wed Aug 30 15:38:30 2023 -0300

    readme part1

commit 3db8dd8aa10f0bde0cff4bc2e62ed1a990e0c7cd
Author: Ana Caroline Ferreira <ana.ferreira@pti.org.br>
Date:   Wed Aug 30 15:25:54 2023 -0300

    meu primeiro commit
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git remote add origem https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git add --all
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git commit -m "readme part2"
[ana_caroline_ferreira eb6f6c8] readme part2
 1 file changed, 26 insertions(+), 1 deletion(-)
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem ana_caroline_ferreira
Enumerating objects: 9, done.
Counting objects: 100% (9/9), done.
Delta compression using up to 8 threads
Compressing objects: 100% (6/6), done.
Writing objects: 100% (9/9), 1.66 KiB | 850.00 KiB/s, done.
Total 9 (delta 2), reused 0 (delta 0), pack-reused 0
remote: 
remote: To create a merge request for ana_caroline_ferreira, visit:
remote:   https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git/-/merge_requests/new?merge_request%5Bsource_branch%5D=ana_caroline_ferreira
remote: 
To https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git
 * [new branch]      ana_caroline_ferreira -> ana_caroline_ferreira
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git merge main
merge: main - not something we can merge
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git merge main ana_caroline_ferreira
merge: main - not something we can merge
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git merge ana_caroline_ferreira main
merge: main - not something we can merge
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git fetch main
fatal: 'main' does not appear to be a git repository
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git fetch 
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git branchs
git: 'branchs' não é um comando git. Veja 'git --help'.

The most similar command is
        branch
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git branch
* ana_caroline_ferreira
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git branches
git: 'branches' não é um comando git. Veja 'git --help'.
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git branch
* ana_caroline_ferreira
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git merge ana_caroline_ferreira main
merge: main - not something we can merge
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git fetch --all
Fetching origem
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 267 bytes | 267.00 KiB/s, done.
From https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git
 * [new branch]      main       -> origem/main
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git branch
* ana_caroline_ferreira
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git branch -a
* ana_caroline_ferreira
  remotes/origem/ana_caroline_ferreira
  remotes/origem/main
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull origem main
From https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git
 * branch            main       -> FETCH_HEAD
hint: You have divergent branches and need to specify how to reconcile them.
hint: You can do so by running one of the following commands sometime before
hint: your next pull:
hint: 
hint:   git config pull.rebase false  # merge (the default strategy)
hint:   git config pull.rebase true   # rebase
hint:   git config pull.ff only       # fast-forward only
hint: 
hint: You can replace "git config" with "git config --global" to set a default
hint: preference for all repositories. You can also pass --rebase, --no-rebase,
hint: or --ff-only on the command line to override the configured default per
hint: invocation.
fatal: Need to specify how to reconcile divergent branches.
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ ^C
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull origem origem/main
fatal: couldn't find remote ref origem/main
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git branch -a
* ana_caroline_ferreira
  remotes/origem/ana_caroline_ferreira
  remotes/origem/main
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull origem remotes/origem/main
fatal: couldn't find remote ref remotes/origem/main
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git config username ana.ferreira
error: key does not contain a section: username
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git config user.name Ana Caroline Ferreira
uso: git config [<options>]

Config file location
    --global              use global config file
    --system              use system config file
    --local               use repository config file
    --worktree            use per-worktree config file
    -f, --file <arquivo>  use given config file
    --blob <blob-id>      read config from given blob object

Action
    --get                 get value: name [value-pattern]
    --get-all             get all values: key [value-pattern]
    --get-regexp          get values for regexp: name-regex [value-pattern]
    --get-urlmatch        get value specific for the URL: section[.var] URL
    --replace-all         replace all matching variables: name value [value-pattern]
    --add                 add a new variable: name value
    --unset               remove a variable: name [value-pattern]
    --unset-all           remove all matches: name [value-pattern]
    --rename-section      rename section: old-name new-name
    --remove-section      remove a section: name
    -l, --list            list all
    --fixed-value         use string equality when comparing values to 'value-pattern'
    -e, --edit            open an editor
    --get-color           find the color configured: slot [default]
    --get-colorbool       find the color setting: slot [stdout-is-tty]

Type
    -t, --type <>         value is given this type
    --bool                value is "true" or "false"
    --int                 value is decimal number
    --bool-or-int         value is --bool or --int
    --bool-or-str         value is --bool or string
    --path                value is a path (file or directory name)
    --expiry-date         value is an expiry date

Other
    -z, --null            terminate values with NUL byte
    --name-only           show variable names only
    --includes            respect include directives on lookup
    --show-origin         show origin of config (file, standard input, blob, command line)
    --show-scope          show scope of config (worktree, local, global, system, command)
    --default <value>     with --get, use default value when missing entry

ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git config user.name "Ana Caroline Ferreira"
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git config user.email "ana.ferreira@pti.org.br"
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull origem main
From https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git
 * branch            main       -> FETCH_HEAD
hint: You have divergent branches and need to specify how to reconcile them.
hint: You can do so by running one of the following commands sometime before
hint: your next pull:
hint: 
hint:   git config pull.rebase false  # merge (the default strategy)
hint:   git config pull.rebase true   # rebase
hint:   git config pull.ff only       # fast-forward only
hint: 
hint: You can replace "git config" with "git config --global" to set a default
hint: preference for all repositories. You can also pass --rebase, --no-rebase,
hint: or --ff-only on the command line to override the configured default per
hint: invocation.
fatal: Need to specify how to reconcile divergent branches.
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git config pull.ff only
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull origem main
From https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git
 * branch            main       -> FETCH_HEAD
fatal: Not possible to fast-forward, aborting.
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git config --global pull.rebase true
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull origem main
error: cannot pull with rebase: You have unstaged changes.
error: please commit or stash them.
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git config --global ]user.email "ana.ferreira@pti.org.br"
error: invalid key: ]user.email
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git config --global user.email "ana.ferreira@pti.org.br"
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git config --global user.name "Ana Caroline Ferreira"
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull origem main
error: cannot pull with rebase: You have unstaged changes.
error: please commit or stash them.
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git config --global list
error: key does not contain a section: list
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git config --global --list
user.email=ana.ferreira@pti.org.br
user.name=Ana Caroline Ferreira
http.cookiefile=/home/ana/.gitcookies
pull.rebase=true
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ rm ~/.gitconfig
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git config --global --list
fatal: unable to read config file '/home/ana/.gitconfig': Arquivo ou diretório inexistente
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull origem main
From https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git
 * branch            main       -> FETCH_HEAD
fatal: Not possible to fast-forward, aborting.
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git config --global --list
fatal: unable to read config file '/home/ana/.gitconfig': Arquivo ou diretório inexistente
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git --rebase pull origem main
unknown option: --rebase
usage: git [--version] [--help] [-C <path>] [-c <name>=<value>]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p | --paginate | -P | --no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           [--super-prefix=<path>] [--config-env=<name>=<envvar>]
           <command> [<args>]
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull --rebase --autostash
There is no tracking information for the current branch.
Please specify which branch you want to rebase against.
See git-pull(1) for details.

    git pull <remote> <branch>

If you wish to set tracking information for this branch you can do so with:

    git branch --set-upstream-to=<remote>/<branch> ana_caroline_ferreira

ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git merge main
merge: main - not something we can merge

Você quis dizer isso?
        origem/main
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git merge origem/main
fatal: refusing to merge unrelated histories
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ ^C
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git merge --allow-unrelated-histories origem/main
error: Your local changes to the following files would be overwritten by merge:
        README.md
Please commit your changes or stash them before you merge.
Aborting
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git add --all
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git commit -m "curso part3"
[ana_caroline_ferreira fef66a3] curso part3
 1 file changed, 51 insertions(+)
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git merge --allow-unrelated-histories origem/main
Mesclagem automática de README.md
CONFLITO (adicionar/adicionar): conflito de mesclagem em README.md
Automatic merge failed; fix conflicts and then commit the result.
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git add --all
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git commit -m "curso part3"
[ana_caroline_ferreira e6bf0b4] curso part3
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem main
error: src refspec main does not match any
error: failed to push some refs to 'https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git'
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem origem/main
remote: HTTP Basic: Access denied
fatal: Authentication failed for 'https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git/'
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem origem/main
Total 0 (delta 0), reused 0 (delta 0), pack-reused 0
To https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git
 ! [remote rejected] origem/main -> origem/main (deny updating a hidden ref)
error: failed to push some refs to 'https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git'
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull origem ana_caroline_ferreira
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 398 bytes | 398.00 KiB/s, done.
From https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git
 * branch            ana_caroline_ferreira -> FETCH_HEAD
   eb6f6c8..b1ff64f  ana_caroline_ferreira -> origem/ana_caroline_ferreira
fatal: Not possible to fast-forward, aborting.
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git fetch --all
Fetching origem
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull origem ana_caroline_ferreira
From https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git
 * branch            ana_caroline_ferreira -> FETCH_HEAD
fatal: Not possible to fast-forward, aborting.
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem ana_caroline_ferreira
remote: HTTP Basic: Access denied
fatal: Authentication failed for 'https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git/'
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git branch -all
error: did you mean `--all` (with two dashes)?
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git branch --all
* ana_caroline_ferreira
  remotes/origem/ana_caroline_ferreira
  remotes/origem/main
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull origem origem/ana_caroline_ferreira
fatal: couldn't find remote ref origem/ana_caroline_ferreira
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git merge --allow-unrelated-histories origem/ana_caroline_ferreira
Mesclagem automática de README.md
CONFLITO (conteúdo): conflito de mesclagem em README.md
Automatic merge failed; fix conflicts and then commit the result.
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git add --all
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git commit -m "curso part4"
[ana_caroline_ferreira 80c357f] curso part4
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem ana_caroline_ferreira
Enumerating objects: 15, done.
Counting objects: 100% (15/15), done.
Delta compression using up to 8 threads
Compressing objects: 100% (6/6), done.
Writing objects: 100% (9/9), 1.67 KiB | 857.00 KiB/s, done.
Total 9 (delta 3), reused 0 (delta 0), pack-reused 0
remote: 
remote: View merge request for ana_caroline_ferreira:
remote:   https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git/-/merge_requests/1
remote: 
To https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git
   b1ff64f..80c357f  ana_caroline_ferreira -> ana_caroline_ferreira
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git add --all
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git commit -m "curso part4"
[ana_caroline_ferreira 5b8c7ac] curso part4
 1 file changed, 1 insertion(+)
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem ana_caroline_ferreira
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 298 bytes | 298.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
remote: 
remote: View merge request for ana_caroline_ferreira:
remote:   https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git/-/merge_requests/1
remote: 
To https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git
   80c357f..5b8c7ac  ana_caroline_ferreira -> ana_caroline_ferreira
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git add --all
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem ana_caroline_ferreira
remote: HTTP Basic: Access denied
fatal: Authentication failed for 'https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git/'
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git commit -m "curso part4"
No ramo ana_caroline_ferreira
nothing to commit, working tree clean
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem ana_caroline_ferreira
Everything up-to-date
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git add --all
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git commit -m "curso part4"
[ana_caroline_ferreira 96203f4] curso part4
 1 file changed, 3 insertions(+), 3 deletions(-)
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem ana_caroline_ferreira
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 304 bytes | 304.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
remote: 
remote: To create a merge request for ana_caroline_ferreira, visit:
remote:   https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git/-/merge_requests/new?merge_request%5Bsource_branch%5D=ana_caroline_ferreira
remote: 
To https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git
   5b8c7ac..96203f4  ana_caroline_ferreira -> ana_caroline_ferreira
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git add --all
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git commit -m "curso part4"
[ana_caroline_ferreira f058739] curso part4
 1 file changed, 13 insertions(+), 9 deletions(-)
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem ana_caroline_ferreira
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 346 bytes | 346.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
remote: 
remote: To create a merge request for ana_caroline_ferreira, visit:
remote:   https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git/-/merge_requests/new?merge_request%5Bsource_branch%5D=ana_caroline_ferreira
remote: 
To https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git
   96203f4..f058739  ana_caroline_ferreira -> ana_caroline_ferreira
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git add --all
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git commit -m "curso part4"
[ana_caroline_ferreira 63f95df] curso part4
 1 file changed, 4 deletions(-)
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem ana_caroline_ferreira
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 299 bytes | 299.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
remote: 
remote: To create a merge request for ana_caroline_ferreira, visit:
remote:   https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git/-/merge_requests/new?merge_request%5Bsource_branch%5D=ana_caroline_ferreira
remote: 
To https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git
   f058739..63f95df  ana_caroline_ferreira -> ana_caroline_ferreira
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git merge main
merge: main - not something we can merge

Você quis dizer isso?
        origem/main
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git merge origem/main
Already up to date.
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull main
fatal: 'main' does not appear to be a git repository
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull origem main
remote: Enumerating objects: 44, done.
remote: Counting objects: 100% (44/44), done.
remote: Compressing objects: 100% (27/27), done.
remote: Total 38 (delta 9), reused 34 (delta 7), pack-reused 0
Unpacking objects: 100% (38/38), 71.38 KiB | 268.00 KiB/s, done.
From https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git
 * branch            main       -> FETCH_HEAD
   194a3a8..68dabe5  main       -> origem/main
Updating 63f95df..68dabe5
Fast-forward
 README.md                                |  43 ++++++++++++++++++++++++++++++++++++++++++-
 imgs/.gitkeep                            |   0
 imgs/cdo-git_clone_vs_fork-f_desktop.png | Bin 0 -> 25740 bytes
 imgs/colaboration.png                    | Bin 0 -> 45205 bytes
 4 files changed, 42 insertions(+), 1 deletion(-)
 create mode 100644 imgs/.gitkeep
 create mode 100644 imgs/cdo-git_clone_vs_fork-f_desktop.png
 create mode 100644 imgs/colaboration.png
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull origem main
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 325 bytes | 325.00 KiB/s, done.
From https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git
 * branch            main       -> FETCH_HEAD
   68dabe5..78f0278  main       -> origem/main
Updating 68dabe5..78f0278
Fast-forward
 README.md | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git tag -a v1.0 -m "curso git 1.0"
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git status
No ramo ana_caroline_ferreira
nothing to commit, working tree clean
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git add --all
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git add --all
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git commit -m "tag"
[ana_caroline_ferreira 9e2b4dd] tag
 1 file changed, 5 insertions(+), 1 deletion(-)
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem ana_caroline_ferreira
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 413 bytes | 413.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
remote: 
remote: To create a merge request for ana_caroline_ferreira, visit:
remote:   https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git/-/merge_requests/new?merge_request%5Bsource_branch%5D=ana_caroline_ferreira
remote: 
To https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git
   63f95df..9e2b4dd  ana_caroline_ferreira -> ana_caroline_ferreira
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem v1.0
Enumerating objects: 1, done.
Counting objects: 100% (1/1), done.
Writing objects: 100% (1/1), 171 bytes | 171.00 KiB/s, done.
Total 1 (delta 0), reused 0 (delta 0), pack-reused 0
To https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git
 * [new tag]         v1.0 -> v1.0
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull] origem ana_caroline_ferreira
git: 'pull]' não é um comando git. Veja 'git --help'.

The most similar command is
        pull
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git pull origem ana_caroline_ferreira
remote: Enumerating objects: 9, done.
remote: Counting objects: 100% (9/9), done.
remote: Compressing objects: 100% (7/7), done.
remote: Total 7 (delta 3), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (7/7), 1.10 KiB | 1.10 MiB/s, done.
From https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git
 * branch            ana_caroline_ferreira -> FETCH_HEAD
   9e2b4dd..1ebe5e1  ana_caroline_ferreira -> origem/ana_caroline_ferreira
Updating 9e2b4dd..1ebe5e1
Fast-forward
 README.md | 15 +++++++++++++++
 1 file changed, 15 insertions(+)
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git add --all
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git commit -m "deveria ignorar algo"
[ana_caroline_ferreira 481c9a7] deveria ignorar algo
 2 files changed, 181 insertions(+), 1 deletion(-)
 create mode 100644 .gitignore
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem ana_caroline_ferreira
Enumerating objects: 6, done.
Counting objects: 100% (6/6), done.
Delta compression using up to 8 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 2.04 KiB | 2.04 MiB/s, done.
Total 4 (delta 1), reused 0 (delta 0), pack-reused 0
remote: 
remote: To create a merge request for ana_caroline_ferreira, visit:
remote:   https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git/-/merge_requests/new?merge_request%5Bsource_branch%5D=ana_caroline_ferreira
remote: 
To https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git
   1ebe5e1..481c9a7  ana_caroline_ferreira -> ana_caroline_ferreira
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git tag -a v1.1 -m "curso git basico até ignore"
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem v.1.1
error: src refspec v.1.1 does not match any
error: failed to push some refs to 'https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git'
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ git push origem v1.1
Enumerating objects: 1, done.
Counting objects: 100% (1/1), done.
Writing objects: 100% (1/1), 182 bytes | 182.00 KiB/s, done.
Total 1 (delta 0), reused 0 (delta 0), pack-reused 0
To https://git.pti.org.br/it.dt/nit/territorio/cursos_internos/curso_git.git
 * [new tag]         v1.1 -> v1.1
ana@ana-Inspiron-5502:~/Área de Trabalho/curso-git$ 
```
